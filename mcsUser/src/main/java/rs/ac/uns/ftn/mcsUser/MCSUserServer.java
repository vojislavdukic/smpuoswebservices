package rs.ac.uns.ftn.mcsUser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(UserService.class)
public class MCSUserServer {
	
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "mcs-user-server");
		SpringApplication.run(MCSUserServer.class, args);
	}
	
}
