package rs.ac.uns.ftn.mscUserRepresentation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserRepresentationService {

	public static void main(String[] args) {
		SpringApplication.run(UserRepresentationService.class, args);
	}
	
}
