package rs.ac.uns.ftn.mcsRegistration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MCSRegistrationServer {

	public static void main(String[] args) {
		System.setProperty("spring.config.name", "mcs-registration-server");
		SpringApplication.run(MCSRegistrationServer.class, args);
	}

}
