package rs.ac.uns.ftn.mscUserRepresentation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableDiscoveryClient
@Import(UserRepresentationService.class)
public class MCSUserRepresentationServer {
	
	public static void main(String[] args) {
		System.setProperty("spring.config.name", "mcs-user-representation-server");
		SpringApplication.run(MCSUserRepresentationServer.class, args);
	}
	
}
