package rs.ac.uns.ftn.mscUserRepresentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
public class HomeController {

	protected static final String userServiceURL = "http://MCS-USER-SERVICE/";
	
	@Autowired
	protected RestTemplate restTemplate;
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/simpleMessageRepresentation")
	@ResponseBody
	public String simpleMessage() {
		return restTemplate.getForObject(userServiceURL+"simpleMessage", String.class);
	}
	
	@RequestMapping("/sendHello/{message}")
	@ResponseBody
	public String sendHelloMessage(@PathVariable String message) {
		return restTemplate.getForObject(userServiceURL+"hello/"+message, String.class);
	}

}
