package rs.ac.uns.ftn.mcsUser.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	@RequestMapping("/simpleMessage")
	public String simpleMessage() {
		return "Hello, world from 'simpleMessage'!";
	}
	
	@RequestMapping("/hello/{userMessage}")
	public String helloWorld(@PathVariable String userMessage) {
		if(userMessage!=null && !userMessage.equals("")){
			return "Hello, " + userMessage + "!";
		}else{
			return "Hello, world!";
		}
	}
	
}
